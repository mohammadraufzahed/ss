#
# Copyright © 2016-2022 The Unalink Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# DB Configuration 
export DATABASE_TS_TYPE=sql
export SPRING_JPA_DATABASE_PLATFORM=org.hibernate.dialect.PostgreSQLDialect
export SPRING_DRIVER_CLASS_NAME=org.postgresql.Driver
export SPRING_DATASOURCE_URL=jdbc:postgresql://thingsboard_db:5432/thingsboard
export SPRING_DATASOURCE_USERNAME=thingsboard
export SPRING_DATASOURCE_PASSWORD=thingsboard
# Specify partitioning size for timestamp key-value storage. Allowed values: DAYS, MONTHS, YEARS, INDEFINITE.
export SQL_POSTGRES_TS_KV_PARTITIONING=MONTHS
export UPDATES_ENABLED=false
export AUDIT_LOG_MASK_DEVICE=RW
export AUDIT_LOG_MASK_ASSET=RW
export AUDIT_LOG_MASK_DASHBOARD=RW
export AUDIT_LOG_MASK_CUSTOMER=RW
export AUDIT_LOG_MASK_USER=RW
export AUDIT_LOG_MASK_RULE_CHAIN=RW
export AUDIT_LOG_MASK_ALARM=RW
export AUDIT_LOG_MASK_INTEGRATION=RW
export AUDIT_LOG_MASK_CONVERTER=RW
export AUDIT_LOG_MASK_ENTITY_GROUP=RW
export AUDIT_LOG_MASK_SCHEDULER_EVENT=RW
export AUDIT_LOG_MASK_BLOB_ENTITY=RW
export AUDIT_LOG_MASK_ENTITY_VIEW=RW
export AUDIT_LOG_MASK_DEVICE_PROFILE=RW
export AUDIT_LOG_MASK_EDGE=RW
export AUDIT_LOG_MASK_RESOURCE=RW
export AUDIT_LOG_MASK_OTA_PACKAGE=RW
export AUDIT_LOG_MASK_ROLE=RW
export AUDIT_LOG_MASK_GROUP_PERMISSION=RW
export HTTP_BIND_PORT=8000
