#### Node text function

<div class="divider"></div>
<br/>

_function (nodeCtx): string_

A JavaScript function used to compute text or HTML code for the current node.

**Parameters:**

<ul>
  <li><b>nodeCtx:</b> <code><a href="https://unalink.net">HierarchyNodeContext</a></code> - An 
            <a href="https://unalink.net">HierarchyNodeContext</a> object
            containing <code>entity</code> field holding basic entity properties <br> (ex. <code>id</code>, <code>name</code>, <code>label</code>) and <code>data</code> field holding other entity attributes/timeseries declared in widget datasource configuration.
   </li>
</ul>

**Returns:**

Should return string value presenting text or HTML for the current node.

<div class="divider"></div>

##### Examples

- Display entity name and optionally temperature value if it is present in entity attributes/timeseries:

```javascript
var data =  nodeCtx.data;
var entity = nodeCtx.entity;
var text = entity.name;
if (data.hasOwnProperty('temperature') && data['temperature'] !== null) {
  text += " <b>"+ data['temperature'] +" °C</b>";
}
return text;
{:copy-code}
```

<br>
<br>
